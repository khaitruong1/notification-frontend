import {
	ON_WORKSPACE_NOTIFICATION_EVENT,
	SUBSCRIBE_WORKSPACE_NOTIFICATION_EVENT,
	WEBSOCKET_ENDPOINT,
} from './constants.js'

const workspaceId = '0v-2f9lPpgcW'

const socket = io(WEBSOCKET_ENDPOINT)

socket.on(ON_WORKSPACE_NOTIFICATION_EVENT, (payload) => {
	console.log(payload)
})

socket.on('connect', () => {
	console.log(socket.id)
	// Subscribe to workspace notification
	socket.emit(SUBSCRIBE_WORKSPACE_NOTIFICATION_EVENT, { workspaceId })
})
