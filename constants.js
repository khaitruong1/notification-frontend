export const WEBSOCKET_ENDPOINT = 'ws://localhost:25004'

export const SUBSCRIBE_WORKSPACE_NOTIFICATION_EVENT =
	'subscribeWorkspaceNotification'
export const ON_WORKSPACE_NOTIFICATION_EVENT = 'onWorkspaceNotification'
